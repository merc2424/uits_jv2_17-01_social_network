
package uits.jv.multilevel.domain.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import uits.jv.multilevel.domain.main.HibernateUtil;

/**
 *
 * @author Merc
 */
public abstract class AbstractDao<T, I extends Serializable> implements BaseDao<T, I>{
    
    private final Class<T> entityClass;
       
    protected Session getCurrentSession(){
        return HibernateUtil.getSessionFactory().getCurrentSession();
    }
    
    public AbstractDao(){
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public T create(T object){
        Session session = getCurrentSession();
        return (T) session.save(object);
    }
    
    @Override
    public boolean remove(T object){
        Session session = getCurrentSession();
        session.delete(object);
        return true;
    }
    
    @Override
    public boolean update(T object){
        Session session = getCurrentSession();
        session.update(object);
        return true;
    }    
    
    @Override
    public List<T> getlist(){
        Session session = getCurrentSession();
        return (List<T>) session.createCriteria(entityClass).list();
    }

    @Override
    public boolean deleteById(Class type, Serializable id){
        Session session = getCurrentSession();
        Transaction t = session.getTransaction();
        t.begin();
        Object persistentInstance = session.load(type, id);
        if (persistentInstance != null) {
            session.delete(persistentInstance);
            t.commit();
            return true;
        }
        return false;
    };

    @Override
    public T getById(I id){
        Session session = getCurrentSession();
        return (T) session.load(entityClass, id);
    }
    
    
    
}
