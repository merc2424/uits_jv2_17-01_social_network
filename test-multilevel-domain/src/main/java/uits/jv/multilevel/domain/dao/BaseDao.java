/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.multilevel.domain.dao;

import java.io.Serializable;
import java.util.List;

public interface BaseDao <T, I> {
    T getById(I key);
    T create (T o);
    boolean update (T o);
    boolean deleteById(Class<?> type, Serializable id);
    boolean remove(T object);
    List<T> getlist();
}
