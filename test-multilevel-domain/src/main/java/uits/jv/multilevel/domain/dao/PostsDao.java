/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.multilevel.domain.dao;

import java.util.List;
import uits.jv.multilevel.domain.entity.Posts;
import uits.jv.multilevel.domain.entity.Users;

public interface PostsDao {
    List<Posts> getByUser(Users users);
    
}

