/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.multilevel.domain.dao;

import java.util.ArrayList;
import uits.jv.multilevel.domain.entity.Users;

/**
 *
 * @author Merc
 */
public interface UsersDao extends BaseDao<Users, Integer>{
Users getByUsername(String username);
Users getByLogin(String login);
boolean isUserExist(String login);
//ArrayList<Users> getAllUsers();

    
}


