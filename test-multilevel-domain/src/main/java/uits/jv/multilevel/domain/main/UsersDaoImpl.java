package uits.jv.multilevel.domain.main;

import uits.jv.multilevel.domain.dao.UsersDao;
import uits.jv.multilevel.domain.entity.Users;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import uits.jv.multilevel.domain.dao.AbstractDao;

public class UsersDaoImpl extends AbstractDao<Users, Integer> implements UsersDao {

    @Override
    public Users getByLogin(String login) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Users.class).add(Restrictions.eq("login", login));
        List<Users> users = criteria.list();
        return (users.size() > 0) ? users.get(0) : null;
    }

    @Override
    public Users getByUsername(String firstname) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Users.class)
                .add(Restrictions.eq("firstname", firstname));
        List<Users> users = criteria.list();
        return (users.size() > 0) ? users.get(0) : null;
    }

    @Override
    public boolean isUserExist(String login) {
        Users user = getByLogin(login);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Users create(Users o) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            session.save(o);
            t.commit();
        } catch (Exception e) {
            t.rollback();
            throw new RuntimeException("Cannot save user", e);
        }
        return o;
    }

    @Override
    public boolean update(Users user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.setFlushMode(FlushMode.AUTO);
        Transaction t = session.beginTransaction();
        try {
            session.merge(user);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            e.printStackTrace();
            return false;
        }

    }

    //    @Override
//    public boolean remove(Users o) {
//        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//        Transaction t = session.beginTransaction();
//        try{
//            session.delete(o.getById(o.getId()));
//            t.commit();
//            return true;
//        } catch(Exception e){
//            t.rollback();
//            throw new RuntimeException("Cannot remove user", e);
//        }         
    //  }
    @Override
    public List<Users> getlist() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Users> users = session.createCriteria(Users.class).list();
        return users;
    }

    @Override
    public Users getById(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Users.class)
                .add(Restrictions.eq("id", id));
        List<Users> users = criteria.list();
        return (users.size() > 0) ? users.get(0) : null;
    }

}
