package uits.jv.multilevel.domain.service;

import uits.jv.multilevel.domain.main.UsersDaoImpl;
import uits.jv.multilevel.domain.dao.BaseDao;
import uits.jv.multilevel.domain.dao.UsersDao;
import uits.jv.multilevel.domain.entity.Posts;
import uits.jv.multilevel.domain.entity.Users;

public class UsersService {

    private UsersDao usersDao;
    private BaseDao<Posts, Integer> posts;

    public UsersService() {
        usersDao = new UsersDaoImpl();
    }

    public UsersService(UsersDao usersDao, BaseDao<Posts, Integer> posts) {
        this();
        this.usersDao = usersDao;
        this.posts = posts;
    }
    
    public boolean isUserExist(String login, String password){
        return usersDao.isUserExist(login);
    }
    
    public Users getByLogin(String login) {
       return usersDao.getByLogin(login);
    }
    
    public Users getById(int id){
        return usersDao.getById(id);
    }
    
    
}
