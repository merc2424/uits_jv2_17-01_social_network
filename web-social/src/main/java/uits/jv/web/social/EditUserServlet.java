/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.web.social;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uits.jv.multilevel.domain.entity.Users;
import uits.jv.multilevel.domain.main.UsersDaoImpl;
import uits.jv.multilevel.domain.service.UsersService;

/**
 *
 * @author Merc
 */
@WebServlet(name = "EditUserServlet", urlPatterns = {"/edit"})
public class EditUserServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userIdS = req.getParameter("id");
        if (userIdS != null) {
            Integer userId = Integer.parseInt(userIdS);

            Users user = new UsersService().getById(userId);

            req.setAttribute("userForEdit", user);
            req.getRequestDispatcher("WEB-INF/views/edituser.jsp").forward(req, resp);

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> errors = new ArrayList<>();
        String userIdS = req.getParameter("id");
        Integer userId;
        try {
            userId = Integer.parseInt(userIdS);
        } catch (NumberFormatException ex) {
            resp.sendError(404, "User not found");
            return;
        }

        Users user = new UsersService().getById(userId);
        if (user != null) {
            String firstname = req.getParameter("firstname");
            String lastname = req.getParameter("lastname");
            if (firstname == null || firstname.isEmpty()) {
                errors.add("Firstname cannot be empty");
                return;
            }
            if (lastname == null || lastname.isEmpty()) {
                errors.add("Lastname cannot be empty");
                return;
            }
            if (!errors.isEmpty()) {
                req.setAttribute("errors", errors);
                doGet(req, resp);
            }

            user.setFirstname(firstname);
            user.setLastname(lastname);
            new UsersDaoImpl().update(user);
            errors.add("User is updated");
            req.setAttribute("errors", errors);
            doGet(req, resp);

        } else {

        }

    }

}
