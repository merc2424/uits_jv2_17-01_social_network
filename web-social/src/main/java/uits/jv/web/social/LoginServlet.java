/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.web.social;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import uits.jv.multilevel.domain.entity.Users;
import uits.jv.multilevel.domain.service.UsersService;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login", "/"})
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Object o = req.getSession().getAttribute("user");
        if (o != null && o instanceof Users) {
            resp.sendRedirect("Userpage");
        } else {
            String notAuthorized = req.getParameter("notAuthorized");
            if (notAuthorized != null) {
                req.setAttribute("errors", Arrays.asList(new String[]{notAuthorized}));
            }
            req.getRequestDispatcher("WEB-INF/views/login.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        List<String> errors = new ArrayList<>();

        if (login.isEmpty()) {
            errors.add("Login cannot be empty");
        }

        if (password.isEmpty()) {
            errors.add("Password cannot be empty");
        }

        if (!errors.isEmpty()) {
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("WEB-INF/views/login.jsp").forward(request, response);
        } else {
            Users user = new UsersService().getByLogin(login);
            if (user != null) {
                if (user.getPassword().equals(password)) {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("user", user);
                    response.addCookie(new Cookie("sid", session.getId()));
                    response.sendRedirect("Userpage");
                } else {
                    errors.add("User not found");
                    request.setAttribute("errors", errors);
                    request.getRequestDispatcher("WEB-INF/views/login.jsp").forward(request, response);
                }
            } else {
                errors.add("User not found");
                request.setAttribute("errors", errors);
                request.getRequestDispatcher("WEB-INF/views/login.jsp").forward(request, response);
            }
        }
    }
}
