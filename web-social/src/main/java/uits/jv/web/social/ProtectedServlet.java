/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.web.social;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import uits.jv.multilevel.domain.dao.UsersDao;
import uits.jv.multilevel.domain.entity.Users;
import uits.jv.multilevel.domain.main.UsersDaoImpl;
import uits.jv.multilevel.domain.service.UsersService;

/**
 *
 * @author Merc
 */
@WebServlet(name = "ProtectedServlet", urlPatterns = {"/Userpage"})
public class ProtectedServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsersDao userrdao = new UsersDaoImpl();
        ArrayList<Users> usersList = (ArrayList<Users>) userrdao.getlist();
        req.setAttribute("usersList", usersList);

        RequestDispatcher rs = req.getRequestDispatcher("WEB-INF/views/userspage.jsp");
        rs.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

}
