/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.web.social;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uits.jv.multilevel.domain.dao.UsersDao;
import uits.jv.multilevel.domain.entity.Users;
import uits.jv.multilevel.domain.main.UsersDaoImpl;

/**
 *
 * @author Merc
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/Register"})
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object o = req.getSession().getAttribute("user");
        if (o != null && o instanceof Users) {
            resp.sendRedirect("Userpage");
        }else {
            req.getRequestDispatcher("WEB-INF/views/register.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String firstName = req.getParameter("firstname");
        String lastName = req.getParameter("lastname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        List<String> errors = new ArrayList<>();

        if (firstName.isEmpty()) {
            errors.add("First Name cannot be empty");
        }

        if (lastName.isEmpty()) {
            errors.add("Last Name cannot be empty");
        }

        if (login.isEmpty()) {
            errors.add("Login cannot be empty");
        }

        if (password.isEmpty()) {
            errors.add("Password cannot be empty");
        }

        if (!errors.isEmpty()) {
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("WEB-INF/views/register.jsp").forward(req, resp);

        } else {
            UsersDao userrdao = new UsersDaoImpl();
            if (!userrdao.isUserExist(login)) {
                Users user = new Users(firstName, lastName, login, password);
                userrdao.create(user);               
                resp.sendRedirect("/web-social/login?notAuthorized=Now please login");
            } else {
                errors.add("User with this login already exists");
                req.setAttribute("errors", errors);
                req.getRequestDispatcher("WEB-INF/views/register.jsp").forward(req, resp);
            }
        }
    }
}
