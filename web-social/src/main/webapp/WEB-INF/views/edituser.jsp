<%-- 
    Document   : edituser
    Created on : Jul 24, 2017, 12:24:36 PM
    Author     : Merc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <body>
        <table align="center" border="0" cellspacing="0" cellpading="0">
            <tr height="20">
                <td colspan="2">
                    <c:if test="${user != null}">
                        <ul> Hello <c:out value="${user.firstname}"/>!</ul>
                    </c:if>
                </td>
                <td>    
                    <a href="/web-social/logout"> <h5>Logout</h5></a> 
                </td>
            </tr>
        </table>
        <form action="" method="POST">
            <table align="center" border="0" cellspacing="0" cellpading="0">
                <tr> <td> <c:if test="${errors != null}">
                            <ul>
                                <c:forEach var="error" items="${errors}">
                                    <li>${error}</li>
                                    </c:forEach>
                            </ul>
                        </c:if>
                    </td>
                </tr>
            </table>

            <table align="center" border="1" cellspacing="2" cellpading="2">

                <c:if test="${userForEdit != null}">
                    <tr>
                        <td>${userForEdit.role}</td>
                    </tr>
                    <tr>
                        <td>${userForEdit.login}</td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="firstname" value="${userForEdit.firstname}"</td>
                    </tr>
                    <td> <input type="text" name="lastname" value="${userForEdit.lastname}"</td>
                    </tr>
                    <tr>
                        <td> <input type="submit" value="Save"</td>
                    </tr>
                </c:if> 
            </table>

            <table align="center" border="0" cellspacing="0" cellpading="0">
                <tr>
                    <td><a href="/web-social/Userpage">Back to all Users</a></td>
                </tr>
            </table>   
        </form>
    </body>
</html>

