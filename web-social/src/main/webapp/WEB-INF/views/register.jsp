<%-- 
    Document   : login
    Created on : Jul 21, 2017, 1:55:47 PM
    Author     : Merc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <title>Register Page</title>
    </head>
    <body>
        <form method="post" action="">
            <table align="center" border="0" cellspacing="0" cellpading="0" bgcolor="#f0f0f0">
                <tr height="40">
                    <td colspan="2">
                        <c:if test="${errors != null}">
                            <ul>
                                <c:forEach var="error" items="${errors}">
                                    <li>${error}</li>
                                    </c:forEach>
                            </ul>
                        </c:if>
                    </td>
                </tr>
                <tr height="20">
                    <td colspan="2" align="center" valign="bottom"><H3>Please fill in all the fields</H3>
                </tr>

                <tr bgcolor="#d0d0d0">
                    <td align="center">FirstName: </td>
                    <td><input type="text" name="firstname" size="20"></td>
                </tr>

                <tr bgcolor="#d0d0d0">
                    <td align="center">LastName: </td>
                    <td><input type="text" name="lastname" size="20"></td>
                </tr>                        

                <tr bgcolor="#d0d0d0">
                    <td align="center">Login: </td>
                    <td><input type="text" name="login" size="20"></td>
                </tr>
                <tr bgcolor="#d0d0d0">
                    <td align="center">Password: </td>
                    <td><input type="password" name="password" size="20"></td>
                </tr>
                <tr height="50">
                    <td colspan="2" align="center"><input type="submit" value="Create" name="create"> <a href="/web-social/login">Back</a> </td>
                </tr>
            </table>
        </form>
    </body>
</html>
