<%-- 
    Document   : userspage
    Created on : Jul 21, 2017, 6:04:33 PM
    Author     : Merc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Users Page</title>
    </head>
    <body>
        <table align="center" border="0" cellspacing="0" cellpading="0">
            <tr height="20">
                <td colspan="2">
                    <c:if test="${user != null}">
                        <ul> Hello <c:out value="${user.firstname}"/>!</ul>
                    </c:if>
                </td>
                <td>    
                    <a href="/web-social/logout"> <h5>Logout</h5></a> 
                </td>
            </tr>
        </table>


        <table align="center" border="1" cellspacing="2" cellpading="2">
            <thead
            <tr>
                <th></th>
                <th>Role</th>
                <th>Login</th>
                <th>Firstname</th>
                <th>Lastname</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${usersList}" var="user">
                <tr>
                    <td><a href="/web-social/edit?id=${user.id}">edit</a></td>
                    <td>${user.role}</td>
                    <td>${user.login}</td>
                    <td>${user.firstname}</td>
                    <td>${user.lastname}</td>
                </tr>
            </c:forEach>   
            </tbody>
        </table>
    </body>
</html>
